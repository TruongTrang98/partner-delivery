import React, { Component } from 'react'
import { Button, Avatar } from 'antd'
import moment from 'moment'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import CreateDishModal from '../../../components/modal/createDishModal'

// import NameFilter from '../../../components/grid-filter/nameFilter'

import './grid.css'

let refDialog = null

class Grid extends Component {
  constructor (props) {
    super(props)
    this.state = { modal: null }
  }

  getRefDialog = (value) => {
    refDialog = value
  }

  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  handleDelete = (data) => {
    this.props.handleDeleteDish(data)
  }

  handleUpdate = (data) => {
    this.props.handleUpdateDish(data)
  }

  render () {
    const imagesLink = process.env.REACT_APP_imagesUrl
    const columnDefs = [
      {
        headerName: 'Hình ảnh',
        field: 'imageUrl',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer', 'text-align': 'center' },
        cellRendererFramework: ({ data }) => (
          <Avatar src={`${imagesLink}/${data.imageUrl}`} size='large' shape='square' />
        )
      },
      {
        headerName: 'Tên món ăn',
        field: 'name',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        filter: 'nameFilter',
        width: 500
      },
      {
        headerName: 'Giá',
        field: 'price',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (
          <span>
            {Intl.NumberFormat('vi-VN', {
              style: 'currency',
              currency: 'VND',
              maximumFractionDigits: 2
            }).format(data.price)}
          </span>
        )
      },
      {
        headerName: 'Phần trăm giảm giá',
        field: 'coupon.percent',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (<span>{!!data.coupon ? `${data.coupon.percent}%` : ``}</span>)
      },
      {
        headerName: 'Giá sau khi giảm',
        field: 'priceAfterCoupon',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer', color: 'red', fontSize: 50 },
        cellRendererFramework: ({ data }) => (
          <span>
            {data.priceAfterCoupon 
              ? Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND',
                maximumFractionDigits: 2
              }).format(data.priceAfterCoupon) : ``}
          </span>
        )
      },
      {
        headerName: 'Ngày tạo',
        field: 'createdAt',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (<span>{moment(+data.createdAt).format('DD-MM-YYYY')}</span>)
      },
      {
        headerName: 'Cập nhật cuối',
        field: 'updatedAt',
        width: 200,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (<span>{moment(+data.updatedAt).format('DD-MM-YYYY')}</span>)
      },
      // {
      //   headerName: 'Tình trạng',
      //   field: 'account.isLocked',
      //   cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
      //   cellRendererFramework: ({ data }) => (
      //     <>
      //       {!data.account.isLocked ? <Tag style={{ cursor: 'pointer' }} color='green'>Đang hoạt động</Tag> : <Tag style={{ cursor: 'pointer' }} color='red'>Không còn hoạt động</Tag>}
      //     </>
      //   )
      // },
      {
        headerName: '',
        field: '',
        // maxWidth: '50',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (<Button style={{ paddingTop: '10' }} type='primary' onClick={() => refDialog.setState({ visible: true, dishInfo: data, type: 'update' })}> Cập nhật </Button>)
      },
      {
        headerName: '',
        field: '',
        // maxWidth: '50',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (<Button style={{ paddingTop: '10' }} type='danger' onClick={() => this.handleDelete(data)}> Xóa </Button>)
      }
    ]
    return (
      <>
        <CreateDishModal getRef={this.getRefDialog} onSubmit={this.props.handleUpdateDish} />
        <div
          className='ag-theme-balham'
          style={{ height: '140%' }}
        >
          <AgGridReact
            columnDefs={columnDefs}
            rowData={this.props.rowData}
            getRowHeight={() => 50}
            animateRows
            onGridReady={this.onGridReady}
            suppressHorizontalScroll
            // domLayout='autoHeight'
            // frameworkComponents={{ nameFilter: NameFilter }}
          />
        </div>
      </>

    )
  }
}
export default Grid
