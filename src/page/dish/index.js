import React from 'react'
import { Button, Empty } from 'antd'
import Grid from './grid/Grid'
import { DISHES, CREATE_DISH, DELETE_DISH, UPDATE_DISH } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import CreateDishModal from '../../components/modal/createDishModal'

let refModal = null

function Dish (props) {
  let { data: { dishes, refetch }, mutate: { createDish, deleteDish, updateDish } } = props
  dishes = dishes.map(item => {
    let priceAfterCoupon
    if (!!item.coupon) {
      priceAfterCoupon = item.price - ((item.price * item.coupon.percent) / 100)
    }
    return { ...item, priceAfterCoupon }
  })
  console.log(dishes)
  localStorage.setItem('path', 'dish')

  function getRefModal (value) {
    refModal = value
  }

  const handleCreate = async (input) => {
    try {
      console.log(input)
      const res = await createDish({
        variables: {
          input
        }
      })
      await refetch()
      console.log(res)
      return new Notify('success', 'Tạo món ăn thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo món ăn thất bại')
    }
  }

  const handleDeleteDish = async (dishInfo) => {
    try {
      const res = await deleteDish({
        variables: {
          _id: dishInfo._id
        }
      })
      await refetch()
      return new Notify('success', 'Xóa món ăn thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Xóa món ăn thất bại')
    }
  }

  const handleUpdateDish = async (_id, input) => {
    // refModal.setState({ visible: true, dishInfo, type: 'update' })
    console.log(_id, input)
    try {
      const res = await updateDish({
        variables: {
          _id,
          input
        }
      })
      await refetch()
      return new Notify('success', 'Cập nhật món ăn thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật món ăn thất bại')
    }
  }

  return (
    <>
      <div style={{ marginBottom: '10px' }}>
        <CreateDishModal getRef={getRefModal} onSubmit={handleCreate} />
        <Button type='primary' onClick={() => refModal.setState({ visible: true, type: 'create' })} icon='plus'>
          Thêm món ăn
        </Button>
      </div>
      <div style={{ width: '80%', height: '55%' }}>
        {dishes.length <= 0 ? (
          <div style={{ width: '100%', height: '60%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Empty description={<span>Không có món ăn</span>} />
          </div>
        ) : (
          <Grid rowData={dishes} handleDeleteDish={(dishInfo) => handleDeleteDish(dishInfo)} handleUpdateDish={handleUpdateDish} />
        )}
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: DISHES,
    name: 'dishes'
  },
  {
    mutation: CREATE_DISH,
    name: 'createDish'
  },
  {
    mutation: UPDATE_DISH,
    name: 'updateDish'
  },
  {
    mutation: DELETE_DISH,
    name: 'deleteDish'
  }
])(Dish)