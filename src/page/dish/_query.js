import gql from 'graphql-tag'

export const DISHES = gql`
  query{
    dishes{
      _id
      name
      imageUrl
      price
      createdAt
      updatedAt
      coupon {
        _id
        content
        percent
      }
    }
  }
`

export const CREATE_DISH = gql`
  mutation ($input: DishInput!){
    createDish(input: $input)
  }
`

export const DELETE_DISH = gql`
  mutation ($_id: String!){
    deleteDish(_id: $_id)
  }
`

export const UPDATE_DISH = gql`
  mutation ($_id: String!, $input: DishInput!) {
    updateDish(_id: $_id, input: $input)
  }
`