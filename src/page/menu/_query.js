import gql from 'graphql-tag'

export const MENU_DATA = gql`
  query menus{
    menus{
      _id
      name
      dishes{
        _id
        name
        imageUrl
        price
        coupon {
          _id
          content
          percent
        }
        createdAt
        updatedAt
      }
      isActive
      createdAt
      updatedAt
    }
  }
`

export const CREATE_MENUS = gql`
  mutation ($input: MenuInput!){
    createMenu(input: $input)
  }
`
