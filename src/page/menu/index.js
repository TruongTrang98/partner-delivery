import React, { useState } from 'react'
import { Button, Empty } from 'antd'
import { MENU_DATA, CREATE_MENUS } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import MenuCollapse from './collapse/MenuCollapse'
import FormModal from '../../components/modal/menuFormModal/index'

import './index.less'

let refModal = null
let refMenuModal = null

function Menu (props) {
  console.log(props)
  const [update, setUpdateMenu] = useState(false)
  let { data: { menus, refetch }, mutate: { createMenu } } = props
  // dishes = dishes.map(item => {
  //   let priceAfterCoupon
  //   if (!!item.coupon) {
  //     priceAfterCoupon = item.price - ((item.price * item.coupon.percent) / 100)
  //   }
  //   return { ...item, priceAfterCoupon }
  // })
  localStorage.setItem('path', 'menu')

  function getRefModal (value) {
    refModal = value
  }
  
  function getRefMenuModal (value) {
    refMenuModal = value
  }

  const handleCreate = async (input) => {
    try {
      const res = await createMenu({
        variables: {
          input
        }
      })
      if (res.data.createMenu === '200') {
        await refetch()
        return new Notify('success', 'Tạo menu thành công')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo menu thất bại')
    }
  }

  function handleUpdateMenu (menu) {
    // console.log(refModal)
    setUpdateMenu(menu)
    refMenuModal.setState({
      visible: true
    })
  }

  return (
    <>
      {/* <CreateMenuModal getRef={getRefModal} /> */}
      <FormModal getRef={getRefModal} title='Tạo menu mới' type='create' onSubmit={handleCreate} />
      <Button type='primary' onClick={() => refModal.setState({ visible: true })} icon='plus'>
        Thêm Menu
      </Button>
      <div className='menu-collapse'>
        {menus.length <= 0 ? (
          <div style={{ width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <Empty description={<span>Không có menu</span>} />
          </div>
        ) : (
          <MenuCollapse refetch={props.data.refetch} rowData={menus} getRef={refMenuModal} />
        )}
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: MENU_DATA,
    name: 'menuData'
  },
  {
    mutation: CREATE_MENUS,
    name: 'createMenu'
  }
])(Menu)