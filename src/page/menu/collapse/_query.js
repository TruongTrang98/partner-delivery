import gql from 'graphql-tag'

export const DELETE_DISH_IN_MENU = gql`
  mutation ($menuId: String!, $dishId: String!){
    deleteDishInMenu(menuId: $menuId, dishId: $dishId)
  }
`

export const DELETE_MENU = gql`
  mutation ($menuId: String!){
    deleteMenu(_id: $menuId)
  }
`

export const UPDATE_MENU = gql`
  mutation ($_id: String!, $input: MenuInput!){
    updateMenu(_id: $_id, input: $input)
  }
`

export const MENU = gql`
    query menu($_id: String!){
      menu (_id: $_id) {
        _id
        name
        dishes{
          _id
          name
          imageUrl
          price
          createdAt
          updatedAt
        }
        isActive
        createdAt
        updatedAt
      },
      dishes{
        _id
        name
        price
        imageUrl
        isActive
        createdAt
        updatedAt
      }
    }
`