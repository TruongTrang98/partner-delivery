import React, { useState } from 'react'
import { Collapse, Icon, Comment, Avatar, Button, Row, Col } from 'antd'
import moment from 'moment'
import { withRouter } from 'react-router-dom'
import { HOCQueryMutation, Notify } from '../../../components/util'
import { DELETE_DISH_IN_MENU, DELETE_MENU, MENU, UPDATE_MENU } from './_query'
import FormModal from '../../../components/modal/menuFormModal/index'
import './MenuCollapse.css'

const { Panel } = Collapse

const customPanelStyle = {
  background: '#f7f7f7',
  borderRadius: 4,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden'
}

let refModal = null

function MenuCollapse (props) {
  console.log(props)
  const { mutate } = props

  const handleDeleteDishInMenu = async (menuId, dishId) => {
    try {
      const res = await mutate.deleteDishInMenu({
        variables: {
          menuId, dishId
        }
      })
      // Refresh
      await props.refetch()
      return new Notify('success', 'Đã xóa món ăn khỏi menu')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Xóa thất bại')
    } 
  }

  const handleDeleteMenu = async (menuId) => {
    try {
      const res = await mutate.deleteMenu({
        variables: {
          menuId
        }
      })
      console.log(res)
      // Refresh
      await props.refetch()
      return new Notify('success', 'Xóa thành công')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Xóa thất bại')
    } 
  }

  const getRefModal = (value) => {
    refModal = value
  }

  const onUpdateClick = (_id) => {
    props.client.query({
      query: MENU,
      variables: {
        _id
      }
    }).then(res => {
      refModal.setState({
        data: res.data.menu,
        visible: true
      })
    }).catch(err => {
      console.error(err)
    })
  }

  const handleUpdateMenu = async (_id, input) => {
    try {
      const res = await mutate.updateMenu({
        variables: {
          _id,
          input
        }
      })
      if (res.data.updateMenu === '200') {
        await props.refetch()
        return new Notify('success', 'Cập nhật thành công')
      } else {
        return new Notify('error', 'Cập nhật thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật thất bại')
    }
  }

  const imagesLink = process.env.REACT_APP_imagesUrl

  return (
    <>
      <FormModal getRef={getRefModal} title='Cập nhật menu' update onSubmit={handleUpdateMenu} />
      <Collapse
        bordered={false}
        // defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <Icon type='caret-right' rotate={isActive ? 90 : 0} />}
      >
        {props.rowData.map((menu, index) => (
          <Panel header={menu.name} key={index} style={customPanelStyle}>
            <Button type='primary' style={{ marginBottom: 10 }} onClick={() => onUpdateClick(menu._id)}> Cập nhật Menu </Button>
            &nbsp; &nbsp;
            <Button type='danger' style={{ marginBottom: 10 }} onClick={() => handleDeleteMenu(menu._id)}> Xóa Menu </Button>
            {menu.dishes.map((dish, idx) => (
              <Comment
                key={idx}
                style={{ background: 'white' }}
                avatar={
                  (
                    <Avatar
                      style={{ width: '100%', height: 90, borderRadius: 0 }}
                      src={`${imagesLink}/${dish.imageUrl}`}
                      alt=''
                    />
                  )
                }
                content={
                  (
                    <div>
                      <Row>
                        <Col span={22}>
                          <h1>
                            {dish.name}
                          </h1>
                          <p>
                            {!!dish.coupon ? (
                              <span>
                                <span style={{ textDecoration: 'line-through' }}>
                                  {Intl.NumberFormat('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                    maximumFractionDigits: 2
                                  }).format(dish.price)}
                                </span>
                                &nbsp;&nbsp;
                                <span style={{ color: 'red' }}>
                                  {Intl.NumberFormat('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                    maximumFractionDigits: 2
                                  }).format(dish.price - (dish.price * dish.coupon.percent) / 100)}
                                </span>
                              </span>
                            ) : (
                              <span>
                                {Intl.NumberFormat('vi-VN', {
                                  style: 'currency',
                                  currency: 'VND',
                                  maximumFractionDigits: 2
                                }).format(dish.price)}
                              </span>
                            )}    
                          </p>
                          <p>
                            Cập nhật ngày: &nbsp;
                            {moment(+dish.updatedAt).format('DD/MM/YYYY')}
                          </p>
                        </Col>
                        <Col span={2}>
                          <div style={{ marginTop: 25 }}>
                            <Button title='Xóa' type='danger' onClick={() => handleDeleteDishInMenu(menu._id, dish._id)}>Xóa</Button>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  )
                }
              />
            ))}
          </Panel>
        ))}
      </Collapse>
    </>
  )
}

export default HOCQueryMutation([
  {
    mutation: DELETE_DISH_IN_MENU,
    name: 'deleteDishInMenu'
  },
  {
    mutation: DELETE_MENU,
    name: 'deleteMenu'
  },
  {
    mutation: UPDATE_MENU,
    name: 'updateMenu'
  }
])(MenuCollapse)