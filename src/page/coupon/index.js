import React, { useState } from 'react'
import { Button } from 'antd'
import gql from 'graphql-tag'
import { CREATE_COUPON, COUPONS } from './_query'
import { HOCQueryMutation, Notify } from '../../components/util'
import CouponCollapse from './collapse/CouponCollapse'
import CouponFormModal from '../../components/modal/couponFormModal/index'
import { COUPON } from './collapse/_query'


let refModal = null
let refCouponModal = null

function Coupon (props) {
  const [update, setUpdateCoupon] = useState(false)
  const { data: { coupons, refetch }, mutate: { createCoupon } } = props
  localStorage.setItem('path', 'coupon')

  function getRefModal (value) {
    refModal = value
  }
  
  function getRefCouponModal (value) {
    refCouponModal = value
  }

  const handleCreate = async (input) => {
    try {
      input.percent = input.percent.toString()
      const res = await createCoupon({
        variables: {
          input
        }
      })
      if (res.data.createCoupon === '200') {
        await refetch()
        return new Notify('success', 'Tạo chương trình khuyến mãi thành công')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Tạo coupon thất bại')
    }
  }

  function handleUpdateCoupon (coupon) {
    // console.log(refModal)
    setUpdateCoupon(coupon)
    refCouponModal.setState({
      visible: true
    })
  }

  const DISHES_FOR_COUPON = gql`
  query{
    dishesForCoupon{
      _id
      name
      price
      imageUrl
      isActive
      createdAt
      updatedAt
    }
  }
`

  const onUpdateCreateCoupon = () => {
    props.client.query({
      query: DISHES_FOR_COUPON
    }).then(res => {
      refModal.setState({
        data: res.data.dishesForCoupon,
        visible: true
      })
    }).catch(err => {
      console.error(err)
    })
  }

  return (
    <>
      {/* <CreateMenuModal getRef={getRefModal} /> */}
      <CouponFormModal getRef={getRefModal} title='Tạo chương trình giảm giá mới' type='create' onSubmit={handleCreate} />
      <Button type='primary' onClick={() => onUpdateCreateCoupon()} icon='plus'>
        Tạo chương trình giảm giá
      </Button>
      <div style={{ width: '80%', marginTop: 10, marginBottom: 30, overflowY: 'auto' }}>
        <CouponCollapse refetch={props.data.refetch} rowData={coupons} getRef={getRefCouponModal} />
      </div>
    </>
  )
}

export default HOCQueryMutation([
  {
    query: COUPONS,
    name: 'coupons'
  },
  {
    mutation: CREATE_COUPON,
    name: 'createCoupon'
  }
])(Coupon)