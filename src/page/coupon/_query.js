import gql from 'graphql-tag'

export const COUPONS = gql`
  query{
    coupons{
      _id
      content
      percent
      dishes {
        _id
        name
        imageUrl
        price
        shopId
        isActive
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`

export const CREATE_COUPON = gql`
  mutation ($input: CouponInput){
    createCoupon(input: $input)
  }
`
