import React, { useState } from 'react'
import { Collapse, Icon, Comment, Avatar, Button, Row, Col } from 'antd'
import moment from 'moment'
import { withRouter } from 'react-router-dom'
import { HOCQueryMutation, Notify } from '../../../components/util'
import { DELETE_DISH_IN_COUPON, DELETE_COUPON, COUPON, UPDATE_COUPON } from './_query'
import CouponFormModal from '../../../components/modal/couponFormModal/index'
import './CouponCollapse.css'

const { Panel } = Collapse

const customPanelStyle = {  
  borderRadius: 10,
  marginBottom: 24,
  border: 0,
  overflow: 'hidden'
}

let refModal = null

function MenuCollapse (props) {
  const { mutate } = props
  const [loading, setLoading] = useState(false)
  const imageLink = process.env.REACT_APP_imagesUrl

  const handleDeleteDishInCoupon = async (couponId, dishId) => {
    try {
      const res = await mutate.deleteDishInCoupon({
        variables: {
          couponId, dishId
        }
      })
      // Refresh
      await props.refetch()
      return new Notify('success', 'Đã xóa món ăn khỏi chương trình')
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Xóa thất bại')
    } 
  }

  const handleDeleteCoupon = async (couponId) => {
    setLoading(true)
    try {
      const res = await mutate.deleteCoupon({
        variables: {
          couponId
        }
      })
      console.log(res)
      // Refresh
      await props.refetch()
      setLoading(false)
      return new Notify('success', 'Xóa chương trình thành công')
    } catch (error) {
      setLoading(false)
      console.error(error)
      return new Notify('error', 'Xóa thất bại')
    } 
  }

  const getRefModal = (value) => {
    refModal = value
  }

  const onUpdateClick = (_id) => {
    props.client.query({
      query: COUPON,
      variables: {
        _id
      }
    }).then(res => {
      refModal.setState({
        data: res.data.coupon,
        visible: true
      })
    }).catch(err => {
      console.error(err)
    })
  }

  const handleUpdateCoupon = async (_id, input) => {
    console.log(_id, input)
    try {
      const res = await mutate.updateCoupon({
        variables: {
          _id,
          input
        }
      })
      if (res.data.updateCoupon === '200') {
        await props.refetch()
        return new Notify('success', 'Cập nhật thành công')
      } else {
        return new Notify('error', 'Cập nhật thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Cập nhật thất bại')
    }
  }

  const imagesLink = process.env.REACT_APP_imagesUrl

  return (
    <>
      <CouponFormModal getRef={getRefModal} title='Cập nhật chương trình giảm giá' update onSubmit={handleUpdateCoupon} />
      <Collapse
        bordered={false}
        // defaultActiveKey={['1']}
        expandIcon={({ isActive }) => <Icon type='caret-right' rotate={isActive ? 90 : 0} />}
      >
        {props.rowData.map((coupon, index) => (
          <Panel style={customPanelStyle} header={`Chương trình: ${coupon.content} - Khuyến mãi ${coupon.percent}%`} key={index}>
            <Button type='primary' style={{ marginBottom: 10 }} onClick={() => onUpdateClick(coupon._id)}> Cập nhật chương trình giảm giá </Button>
            &nbsp; &nbsp;
            <Button type='danger' style={{ marginBottom: 10 }} onClick={() => handleDeleteCoupon(coupon._id)} loading={loading}> Xóa chương trình </Button>
            {coupon.dishes.map((dish, idx) => (
              <Comment
                key={idx}
                style={{ background: 'white' }}
                avatar={
                  (
                    <Avatar
                      style={{ width: '100%', height: 90, borderRadius: 0 }}
                      src={`${imagesLink}/${dish.imageUrl}`}
                      alt=''
                    />
                  )
                }
                content={
                  (
                    <div>
                      <Row>
                        <Col span={22}>
                          <h1>
                            {dish.name}
                          </h1>
                          <p>
                            {Intl.NumberFormat('vi-VN', {
                              style: 'currency',
                              currency: 'VND',
                              maximumFractionDigits: 2
                            }).format(dish.price)}
                          </p>
                          <p>
                            Cập nhật ngày: &nbsp;
                            {moment(+dish.updatedAt).format('DD/MM/YYYY')}
                          </p>
                        </Col>
                        <Col span={2}>
                          <div style={{ marginTop: 25 }}>
                            <Button title='Xóa' type='danger' onClick={() => handleDeleteDishInCoupon(coupon._id, dish._id)}>Xóa</Button>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  )
                }
              />
            ))}
          </Panel>
        ))}
      </Collapse>
    </>
  )
}

export default HOCQueryMutation([
  {
    mutation: DELETE_DISH_IN_COUPON,
    name: 'deleteDishInCoupon'
  },
  {
    mutation: DELETE_COUPON,
    name: 'deleteCoupon'
  },
  {
    mutation: UPDATE_COUPON,
    name: 'updateCoupon'
  }
])(MenuCollapse)