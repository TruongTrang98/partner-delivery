import gql from 'graphql-tag'

export const DELETE_DISH_IN_COUPON = gql`
  mutation ($couponId: String!, $dishId: String!){
    deleteDishInCoupon(couponId: $couponId, dishId: $dishId)
  }
`

export const DELETE_COUPON = gql`
  mutation ($couponId: String!){
    deleteCoupon(_id: $couponId)
  }
`

export const UPDATE_COUPON = gql`
  mutation ($_id: String!, $input: CouponInput!){
    updateCoupon(_id: $_id, input: $input)
  }
`

export const COUPON = gql`
    query coupon($_id: String!){
      coupon (_id: $_id) {
        _id
        content
        percent
        dishes{
          _id
          name
          imageUrl
          price
          couponId
        }
        createdAt
        updatedAt
      },
      dishesForCoupon{
        _id
        name
        price
        imageUrl
        isActive
        createdAt
        updatedAt
      }
    }
`


// export const COUPON = gql`
//     query coupon($_id: String!){
//       coupon (_id: $_id) {
//         _id
//         content
//         percent
//         dishes{
//           _id
//           name
//           imageUrl
//           price
//         }
//         createdAt
//         updatedAt
//       },
//       dishes{
//         _id
//         name
//         price
//         imageUrl
//         isActive
//         createdAt
//         updatedAt
//       }
//     }
// `