import gql from 'graphql-tag'

export const ORDERS = gql`
    query {
      ordersPending{
      _id
      name
      phoneNumber
      address
      costDishes
      user {
        _id
        name
        phoneNumber
        email
      }
      status
      dishes {
        _id
        name
        price
        count
      }
      createdAt
    }
  }
`

export const ORDERS_CREATED = gql`
  subscription {
    ordersCreated {
      _id
      name
      phoneNumber
      address
      costDishes
      user {
        _id
        name
        phoneNumber
        email
      }
      status
      dishes {
        _id
        name
        price
        count
      }
      createdAt
    }
}
`

export const CHANGE_STATUS = gql`
  mutation ($_id: String!, $status: String!) {
    changeStatus (_id: $_id, status: $status)
  }
`