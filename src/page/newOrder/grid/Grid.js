import React, { Component } from 'react'
import { Tag, Empty, Input, Row, Col } from 'antd'
import moment from 'moment'
import { ChevronRight } from 'react-feather'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import PreviewModal from '../../../components/modal/previewModal'
import { HOCQueryMutation, Notify } from '../../../components/util'
import { ORDERS, ORDERS_CREATED, CHANGE_STATUS } from '../_query'
import './grid.less'

class Grid extends Component {
  constructor (props) {
    super(props)
    this.state = { modal: null }
  }

  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  onRowSelected = (lol) => {
    this.state.modal(lol)
  }

  handleReceiveOrder = async _id => {
    const { mutate: { changeStatus }, data: { refetch } } = this.props
    try {
      const orderChangeStatus = await changeStatus({
        variables: {
          _id,
          status: 'shopReceiveOrder'
        }
      })
      if (orderChangeStatus.data.changeStatus === '200') {
        await refetch()
        return new Notify('success', 'Nhận đơn hàng thành công')
      } else {
        return new Notify('error', 'Nhận đơn hàng thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Nhận đơn hàng thất bại')
    }
  }

  handleIgnoreOrder = async _id => {
    const { mutate: { changeStatus }, data: { refetch } } = this.props
    try {
      const orderChangeStatus = await changeStatus({
        variables: {
          _id,
          status: 'cancel'
        }
      })
      if (orderChangeStatus.data.changeStatus === '200') {
        await refetch()
        return new Notify('success', 'Từ chối đơn hàng thành công')
      } else {
        return new Notify('error', 'Từ chối đơn hàng thất bại')
      }
    } catch (error) {
      console.error(error)
      return new Notify('error', 'Từ chối đơn hàng thất bại')
    }
  }

  render () {
    const columnDefs = [
      {
        headerName: 'ID đơn hàng',
        field: '_id',
        width: 300,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Tên khách hàng',
        field: 'name',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Số điện thoại',
        field: 'phoneNumber',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Địa chỉ giao hàng',
        field: 'address',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Ngày tạo',
        field: 'createdAt',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => moment(+data.createdAt).format('DD/MM/YYYY hh:mm')
      },
      {
        headerName: 'Tình trạng',
        field: 'status',
        width: 150,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (
          <>
            <Tag style={{ cursor: 'pointer' }} color='red'>Đang chờ xử lý</Tag>
          </>
        )
      },
      { 
        headerName: '',
        field: '',
        maxWidth: '50',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: () => (<ChevronRight style={{ paddingTop: '10' }} />)
      }
    ]
    const { data: { ordersPending }, ordersCreated } = this.props
    console.log(this.props)
    return (
      <>
        <PreviewModal 
          getOnOpen={(cb) => this.setState({ modal: cb })}
          onReceive={this.handleReceiveOrder}
          onIgnore={this.handleIgnoreOrder}
        />
        {((!ordersCreated.ordersCreated && ordersPending.length <= 0) 
        || (!!ordersCreated.ordersCreated && ordersCreated.ordersCreated.length <= 0))
          ? (
            
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
              <Empty description={<span>Không có đơn hàng</span>} />
            </div>
          ) : (
            <>
              <div 
                className='ag-theme-balham'
                style={{ height: '100%' }} 
              >
                <AgGridReact
                  columnDefs={columnDefs}
                  rowData={!!ordersCreated.ordersCreated ? ordersCreated.ordersCreated : ordersPending} 
                  getRowHeight={() => 50}
                  animateRows
                  onGridReady={this.onGridReady}
                  onRowClicked={this.onRowSelected}
                  suppressHorizontalScroll
                  // frameworkComponents={{ nameFilter: NameFilter }}
                />
              </div>
            </>
          )}
      </>
    )
  }
}
export default HOCQueryMutation([
  {
    query: ORDERS,
    name: 'orders',
    options: {
      fetchPolicy: 'network-only'
    }
  },
  {
    mutation: CHANGE_STATUS,
    name: 'changeStatus'
  },
  {
    subcription: ORDERS_CREATED,
    name: 'ordersCreated'
  }
])(Grid)