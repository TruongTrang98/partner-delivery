import gql from 'graphql-tag'

export const ORDERS = gql`
    query($idSearch: String) {
      ordersShopDone(idSearch: $idSearch){
      _id
      name
      phoneNumber
      address
      costDishes
      user {
        _id
        name
        phoneNumber
        email
      }
      status
      dishes {
        _id
        name
        price
        count
      }
      createdAt
    }
  }
`