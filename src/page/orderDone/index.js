import React from 'react'
import Grid from './grid/Grid'

function Order () {
  localStorage.setItem('path', 'orderDone')
  return (
    <div style={{ width: '80%', height: '80%' }}>
      <Grid />
    </div>
  )
}

export default Order