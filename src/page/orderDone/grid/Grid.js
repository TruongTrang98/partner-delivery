import React, { Component } from 'react'
import { Tag, Empty, Row, Col, Input } from 'antd'
import moment from 'moment'
import { ChevronRight } from 'react-feather'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import PreviewModal from '../../../components/modal/previewModal'
import { HOCQueryMutation, Notify } from '../../../components/util'
import { ORDERS } from '../_query'
import './grid.less'

class Grid extends Component {
  constructor (props) {
    super(props)
    this.state = { modal: null }
  }

  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  onRowSelected = (lol) => {
    this.state.modal(lol)
  }

  onSearch = (idSearch) => {
    this.props.data.refetch({ idSearch })
  }

  render () {
    const columnDefs = [
      {
        headerName: 'ID đơn hàng',
        field: '_id',
        width: 300,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Tên khách hàng',
        field: 'name',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Số điện thoại',
        field: 'phoneNumber',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Địa chỉ giao hàng',
        field: 'address',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Ngày tạo',
        field: 'createdAt',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => moment(+data.createdAt).format('DD/MM/YYYY hh:mm')
      },
      {
        headerName: 'Tình trạng',
        field: 'status',
        width: 150,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (
          <>
            <Tag style={{ cursor: 'pointer' }} color='green'>Đơn hàng hoàn thành</Tag>
          </>
        )
      },
      { 
        headerName: '',
        field: '',
        maxWidth: '50',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: () => (<ChevronRight style={{ paddingTop: '10' }} />)
      }
    ]
    const { ordersShopDone } = this.props.data
    console.log(this.props)
    return (
      <>
        <PreviewModal 
          getOnOpen={(cb) => this.setState({ modal: cb })}
          onReceive={null}
          onIgnore={null}
        />
        <Row style={{ marginBottom: 20 }}>
          <Col span={10}>
            <Input.Search placeholder='Nhập ID đơn hàng để tìm kiếm' onPressEnter={({ target }) => this.onSearch(target.value)} />
          </Col>
        </Row>
        {ordersShopDone.length <= 0 ? (
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
            <Empty description={<span>Không có đơn hàng</span>} />
          </div>
        ) : (
          <>
            <div 
              className='ag-theme-balham'
              style={{ height: '100%' }} 
            >
              <AgGridReact
                columnDefs={columnDefs}
                rowData={ordersShopDone} 
                getRowHeight={() => 50}
                animateRows
                onGridReady={this.onGridReady}
                onRowClicked={this.onRowSelected}
                suppressHorizontalScroll
                // frameworkComponents={{ nameFilter: NameFilter }}
              />
            </div>
          </>
        )}
      </>
    )
  }
}
export default HOCQueryMutation([
  {
    query: ORDERS,
    name: 'orders',
    options: {
      variables: {
        idSearch: ''
      },
      fetchPolicy: 'network-only'
    }
  }
])(Grid)