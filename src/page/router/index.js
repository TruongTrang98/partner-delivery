import React, { Component, Suspense } from 'react'
import { Switch, Redirect, Route, Link } from 'react-router-dom'
import gql from 'graphql-tag'
import { Menu, Icon, Layout, Dropdown, Input, PageHeader } from 'antd'
import { listPrivateRoute, menu } from '../../config/router'
import ChangePasswordModal from '../../components/modal/changePasswordModal'
import InfoModal from '../../components/modal/infoModal'
import { PrivateRoute, WrapLazy, Loading, HOCMobX, Notify, HOCQueryMutation } from '../../components/util'
import './index.less'

const { SubMenu } = Menu
const { Header, Sider, Content } = Layout

const languageMenu = (
  <Menu>
    <Menu.Item>
      Tiếng Việt
    </Menu.Item>
    <Menu.Item>
      English
    </Menu.Item>
  </Menu>
)

let refChangePassword = null
let refInfo = null

class Router extends Component {
  constructor (props) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  toggle = () => {
    this.setState(prevState => ({
      collapsed: !prevState.collapsed
    }))
  }

  handleLogout = () => {
    this.props.Authen.onLogout()
    return new Notify('success', 'Đăng xuất thành công')
  }

  getRefChangePassword = (value) => {
    refChangePassword = value
  }

  getRefInfo = (value) => {
    refInfo = value
  }
  
  iconclick = () => {
    if (this.props.location.pathname !== '/') {
      this.props.history.push('/')
    }
  }

  render () {
    const imageLink = process.env.REACT_APP_imagesUrl
    const { data: { meShop } } = this.props
    console.log(meShop)
    const imageUrl = process.env.REACT_APP_imagesUrl
    return (
      <>
        <Layout style={{ height: window.innerHeight }}>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed} style={{ background: '#fff' }}>
            <div className='logo' onClick={this.iconclick}>
              {
                !this.state.collapsed ? <img src={`${imageUrl}/logo.png`} alt='logo' width='170px' /> : <img src={`${imageUrl}/logo-rut-gon.png`} alt='logo' width='50px' />
              }
            </div>
            <Menu theme='light' mode='inline' defaultSelectedKeys={localStorage.getItem('path')} defaultOpenKeys={['subOrder']}>
              {menu.map((item) => (
                item.subMenu ? (
                  <SubMenu
                    key={item.key}
                    title={(
                      <span>
                        <Icon type={item.icon} />
                        <span>{item.displayName}</span>
                      </span>
                    )}
                  >
                    {item.subMenu.map(sub => (
                      <Menu.Item key={sub.key}>
                        <Link to={sub.path}>
                          <span>{sub.displayName}</span>
                        </Link>
                      </Menu.Item>
                    ))}
                  </SubMenu>
                ) : (
                  <Menu.Item key={item.key}>
                    <Link to={item.path}>
                      <Icon type={item.icon} />
                      <span>{item.displayName}</span>
                    </Link>
                  </Menu.Item>
                )))}
            </Menu>
          </Sider>
          <Layout style={{ height: '100%' }}>
            <PageHeader
              // title={(
              //   <Icon
              //     className='trigger'
              //     type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              //     onClick={this.toggle}
              //   />
              // )}
              // subTitle={(
              //   <Input.Search className='search-bar' placeholder='Search' style={{ width: '70vh' }} />
              // )}
              extra={[
                <Dropdown
                  key='1'
                  overlay={
                    (
                      <Menu>
                        <Menu.Item onClick={() => {
                          refInfo.setState({ visible: true })
                        }}
                        >
                          Profile
                        </Menu.Item>
                        <Menu.Item onClick={() => {
                          if (refChangePassword) {
                            refChangePassword.setState({ visible: true })
                          }
                        }}
                        >
                          Đổi mật khẩu
                        </Menu.Item>
                        <Menu.Item
                          onClick={this.handleLogout}
                        >
                          Đăng xuất
                        </Menu.Item>
                      </Menu>
                    )
                  }
                >
                  <Icon type='user' className='user-icon' />
                </Dropdown>,
                <Dropdown key='2' overlay={languageMenu}>
                  <Icon type='global' className='global-icon' />
                </Dropdown>
              ]}
            />
            <Content
              style={{
                margin: '24px 16px',
                padding: 24,
                background: '#fff'
              }}
            >
              <Suspense fallback={<Loading />}>
                <Switch>
                  {listPrivateRoute.map((route, idx) => (
                    <PrivateRoute
                      key={idx}
                      exact={route.exact}
                      path={route.path}
                      component={WrapLazy(import(`../${route.component}`))(300)}
                      route={route}
                      profile={this.profile}
                    />
                  ))}
                  <Redirect to={`/newOrder`} />
                  <Route component={WrapLazy(import(`../404`))(250)} />
                </Switch>
              </Suspense>
            </Content>
          </Layout>
        </Layout>
        <ChangePasswordModal getRef={this.getRefChangePassword} />
        <InfoModal getRef={this.getRefInfo} />
      </>
    )
  }
}

const WHO_AM_I = gql`
  query{
    meShop {
      _id
      name
      address
      phoneNumber
      email
      imageUrl
    }
  }
`

export default HOCQueryMutation([
  {
    query: WHO_AM_I,
    name: 'whoAmI'
  }
])(HOCMobX(Router)(true))
