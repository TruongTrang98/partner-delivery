import gql from 'graphql-tag'

export const ORDERS = gql`
    query($idSearch: String) {
      ordersReceivedAndShipperReceived(idSearch: $idSearch){
      _id
      name
      address
      phoneNumber
      costDishes
      user {
        _id
        name
        phoneNumber
        email
      }
      status
      dishes {
        _id
        name
        price
        count
      }
      createdAt
    }
  }
`

// export const ORDERS_CREATED = gql`
//   subscription {
//     ordersCreated {
//       _id
//       user {
//         _id
//         name
//         phoneNumber
//         email
//       }
//       status
//       address
//       dishes {
//         _id
//         name
//         price
//         count
//       }
//       createdAt
//     }
// }
// `

// export const CHANGE_STATUS = gql`
//   mutation ($_id: String!, $status: String!) {
//     changeStatus (_id: $_id, status: $status)
//   }
// `