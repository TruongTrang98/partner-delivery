import React, { Component } from 'react'
import { Tag, Empty, Input, Row, Col } from 'antd'
import moment from 'moment'
import { ChevronRight } from 'react-feather'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import PreviewModal from '../../../components/modal/previewModal'
import { HOCQueryMutation, Notify } from '../../../components/util'
import { ORDERS } from '../_query'
import './grid.css'

class Grid extends Component {
  constructor (props) {
    super(props)
    this.state = { modal: null }
  }

  onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }

  onRowSelected = (lol) => {
    this.state.modal(lol)
  }

  onSearch = (txtSearch) => {
    this.props.data.refetch({ idSearch: txtSearch }) 
  }

  render () {
    const columnDefs = [
      {
        headerName: 'ID đơn hàng',
        field: '_id',
        width: 300,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Tên khách hàng',
        field: 'name',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
        // filter: 'nameFilter'
      },
      {
        headerName: 'Số điện thoại',
        field: 'phoneNumber',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Địa chỉ giao hàng',
        field: 'address',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' }
      },
      {
        headerName: 'Ngày tạo',
        field: 'createdAt',
        width: 100,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => moment(+data.createdAt).format('DD/MM/YYYY hh:mm')
      },
      {
        headerName: 'Tình trạng',
        field: 'status',
        width: 150,
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: ({ data }) => (
          <>
            {data.status === 'shopReceiveOrder' 
              ? <Tag style={{ cursor: 'pointer' }} color='orange'>Đơn hàng đã nhận</Tag> 
              : <Tag style={{ cursor: 'pointer' }} color='green'>Shipper đã nhận đơn hàng</Tag>}
          </>
        )
      },
      { 
        headerName: '',
        field: '',
        maxWidth: '50',
        cellStyle: { display: 'flex', 'align-items': 'center', border: 'none', cursor: 'pointer' },
        cellRendererFramework: () => (<ChevronRight style={{ paddingTop: '10' }} />)
      }
    ]
    const { data: { ordersReceivedAndShipperReceived, refetch } } = this.props
    console.log(this.props)
    return (
      <>
        <PreviewModal 
          getOnOpen={(cb) => this.setState({ modal: cb })}
          onReceive={this.handleReceiveOrder}
          onIgnore={this.handleIgnoreOrder}
        />
        <Row style={{ marginBottom: 20 }}>
          <Col span={10}>
            <Input.Search placeholder='Nhâp ID đơn hàng để tìm kiếm' onPressEnter={(e) => this.onSearch(e.target.value)} />
          </Col>
        </Row>
        {ordersReceivedAndShipperReceived.length <= 0 ? (
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
            <Empty description={<span>Không có đơn hàng</span>} />
          </div>
        ) : (
          <>
            <div 
              className='ag-theme-balham'
              style={{ height: '100%' }} 
            >
              <AgGridReact
                columnDefs={columnDefs}
                rowData={ordersReceivedAndShipperReceived} 
                getRowHeight={() => 50}
                animateRows
                onGridReady={this.onGridReady}
                onRowClicked={this.onRowSelected}
                suppressHorizontalScroll
                // domLayout='autoHeight'
                // frameworkComponents={{ nameFilter: NameFilter }}
              />
            </div>
          </>
        )}
      </>
      
    )
  }
}
export default HOCQueryMutation([
  {
    query: ORDERS,
    name: 'ordersReceived',
    options: {
      variables: {
        idSearch: ''
      },
      fetchPolicy: 'network-only'
    }
  }
  // {
  //   query: ORDERS,
  //   name: 'ordersShipperReceived',
  //   options: () => ({
  //     variables: {
  //       status: 'shipperReceived'
  //     }
  //   })
  // }
])(Grid)