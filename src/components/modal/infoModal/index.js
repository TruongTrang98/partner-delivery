import React from 'react'
import { Modal, Row, Col, Avatar } from 'antd'
import gql from 'graphql-tag'

import { HOCQueryMutation } from '../../util'

import './index.less'

class InfoModal extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false
    }
  }

  componentDidMount = () => {
    this.props.getRef(this)
  }

  render () {
    const { meShop } = this.props.data
    const imageUrl = process.env.REACT_APP_imagesUrl
    return (
      <Modal
        title='Thông tin shop'
        visible={this.state.visible}
        onCancel={() => this.setState({ visible: false })}
        footer={null}
      >
        <Row className='shop-info'>
          <Col span={7}>
            <Avatar shape='square' src={`${imageUrl}/${meShop.imageUrl}`} />
          </Col>
          <Col span={17}>
            <Row>
              <Col span={24}>
                <div className='shop-name'>{meShop.name}</div>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div className='shop-address'>{meShop.address}</div>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div className='shop-email'>
                  Email: &nbsp;
                  <span>{meShop.email}</span>
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div className='shop-phoneNumber'>
                  Số điện thoại: &nbsp;
                  <span>{meShop.phoneNumber}</span>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Modal>  
    )
  }
}

const SHOP = gql`
  query {
    meShop {
      _id
      imageUrl
      name
      phoneNumber
      email
      address
      createdAt
      updatedAt
    }
  }
`

export default HOCQueryMutation([
  {
    query: SHOP,
    name: 'shop'
  }
])(InfoModal)