/* eslint-disable */
import React, { useState, useEffect } from 'react'
import { Modal, Tag, Button, Col, Row, Popconfirm } from 'antd'
import moment from 'moment'
import { ChevronLeft, ChevronRight } from 'react-feather'
import { AgGridReact } from 'ag-grid-react'
import 'ag-grid-community/dist/styles/ag-grid.css'
import 'ag-grid-community/dist/styles/ag-theme-balham.css'

import './index.less'
import Grid from 'antd/lib/card/Grid'

const PreviewModal = props => {
  const [state, setState] = useState({
    visible: false,
    content: '',
    data: null,
    row: 0,
    max: 0
  })
  const [loading, setLoading] = useState(false)

  const handlePage = async flag => {
    const row = flag === 'left' ? state.row - 1 : state.row + 1
    const node = state.data[!!state.data.api ? 'api' : 'gridApi'].getDisplayedRowAtIndex(row)
    setState({ ...state, content: node.data.content, row, data: node })
  }
  
  useEffect(() => {
    if (!!props.getOnOpen) {
      props.getOnOpen(data => {
        // console.log(data.data)
        setState({
          ...state,
          visible: true,
          content: data.data.content,
          data,
          row: data.rowIndex,
          max: data.api.getDisplayedRowCount()
        })
      })
    }
  }, [])

  const handleReceive = async () => {
    setLoading(true)
    await props.onReceive(state.data.data._id)
    setLoading(false)
    setState({
      ...state,
      visible: false
    })
  }

  const handleIgnore = async () => {
    setLoading(true)
    await props.onIgnore(state.data.data._id)
    setLoading(false)
    setState({
      ...state,
      visible: false
    })
  }

  const columnDefs = [
    {
      headerName: 'Tên món ăn',
      field: 'name',
      cellStyle: { display: 'flex', 'align-items': 'center', border: 'none' }
      // filter: 'nameFilter'
    },
    {
      headerName: 'Số lượng',
      field: 'count',
      cellStyle: { display: 'flex', 'align-items': 'center', border: 'none' }
      // filter: 'nameFilter'
    },
    {
      headerName: 'Đơn giá',
      field: 'price',
      cellStyle: { display: 'flex', 'align-items': 'center', border: 'none' },
      cellRendererFramework: ({ data }) => (
        <span>
          {Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 2
        }).format(data.price)}
        </span>)
    },
    {
      headerName: 'Thành tiền',
      field: '',
      cellStyle: { display: 'flex', 'align-items': 'center', border: 'none' },
      cellRendererFramework: ({ data }) => (
        <span style={{ color: '#f72d2d' }}>
          {Intl.NumberFormat('vi-VN', {
          style: 'currency',
          currency: 'VND',
          maximumFractionDigits: 2
        }).format(data.count * data.price)}
        </span>
      )
    }
   
  ]

  const onGridReady = (params) => {
    params.api.sizeColumnsToFit()
  }
  console.log(state.data)

  return (
    <>
      <Modal
        visible={state.visible}
        footer={null}
        title={state.visible && state.data !== null && (
					<>
						<div className='boxItem pr30' style={{width: 340}}>
							<div className='title'>ID đơn hàng</div>
							<div className='boxContacts' style={{position: "relative", top: 4, left: 1, color: '#4ABFA9'}}><span>{state.data.data._id}</span></div>
						</div>
						<div className='boxItem ml12 pr30' style={{width: 350}}>
              <div className='title'>Tình trạng đơn hàng</div>
							<div className='boxContacts' style={{position: "relative", top: 4, left: -70}}>
              {state.data.data.status === 'shopReceiveOrder' 
                ? <Tag color='orange'>Đơn hàng đã nhận</Tag>
                : state.data.data.status === 'shipperReceiveOrder'
                ? <Tag color='lime'>Shipper đã nhận đơn hàng</Tag>
                : state.data.data.status === 'pending'
                ? <Tag color='red'>Đang chờ xử lý</Tag>
                : state.data.data.status === 'shipperReceiveFood' || state.data.data.status === 'done'
                ? <Tag color='green'>Đơn hàng hoàn thành</Tag> : null}
                </div>
            </div>
            {/* <div className='boxItem ml12' style={{width: 100}}>
              <div className='title'>SĐT</div>
							<div className='boxContacts' style={{position: "relative", top: 4, left: -6}}><span>{state.data.data.phoneNumber}</span></div>
            </div> */}
					</>
        )}
        onCancel={() => setState({ visible: false })}
				width={860}
				wrapClassName='modal-preview'
				maskStyle={{
          backgroundColor: 'rgba(79, 79, 79, 0.58)'
        }}
        centered={!false}
        style={{height: 330}}
      >
        {state.visible && (
          <div className='content'>
            <Row>
              <Col span={5}>
                <div className='label'>Tên khách hàng: </div>
              </Col>
              <Col span={17}>
                <div className='content-label'>{state.data.data.name}</div>
              </Col>
            </Row>
            <Row>
              <Col span={5}>
                <div className='label'>Địa chỉ giao hàng: </div>
              </Col>
              <Col span={17}>
                <div className='content-label' style={{ color: '#4ABFA9' }}>{state.data.data.address}</div>
              </Col>
            </Row>
            <Row>
              <Col span={5}>
                <div className='label'>Số điện thoại </div>
              </Col>
              <Col span={17}>
                <div className='content-label' style={{ color: '#4ABFA9' }}>{state.data.data.phoneNumber}</div>
              </Col>
            </Row>
            <Row>
              <Col span={5}>
                <div className='label'>Ngày tạo: </div>
              </Col>
              <Col span={17}>
                <div className='content-label'>{moment(+state.data.data.createdAt).format('DD/MM/YYYY hh:mm')}</div>
              </Col>
            </Row>
            <Row>
              <div 
                className='ag-theme-balham'
                style={{ height: '300px' }} 
              >
                <AgGridReact
                  columnDefs={columnDefs}
                  rowData={state.data.data.dishes} 
                  getRowHeight={() => 50}
                  animateRows
                  onGridReady={onGridReady}
                  suppressHorizontalScroll
                  // domLayout='autoHeight'
                  // frameworkComponents={{ nameFilter: NameFilter }}
                />
              </div>
            </Row>
            <Row>
              <Col span={5} offset={14}>
                <div className='label'>Thành tiền: </div>
              </Col>
              <Col span={4}>
                <div className='content-label' style={{ color: '#f72d2d' }}>
                {Intl.NumberFormat('vi-VN', {
                  style: 'currency',
                  currency: 'VND',
                  maximumFractionDigits: 2
                }).format(+state.data.data.costDishes)}
                </div>
              </Col>
            </Row>
            {state.data.data.status === 'pending' 
            && (
                <div className='' style={{position:"absolute", right: 20, bottom: 10}}>
                  <Popconfirm
                    title="Bạn thật sự muốn nhận đơn hàng này?"
                    onConfirm={handleReceive}
                    okText="Có"
                    cancelText="Không"
                  >
                    <Button loading={loading} className='btn-received' style={{ marginRight: '20px' }} type='primary' size='large'>Nhận đơn hàng</Button>
                  </Popconfirm>
                  <Popconfirm
                    title="Bạn thật sự muốn từ chối đơn hàng này?"
                    onConfirm={handleIgnore}
                    okText="Có"
                    cancelText="Không"
                  >
                    <Button loading={loading} className='btn-ignore' type='danger' size='large'>Từ chối đơn hàng</Button>
                  </Popconfirm>
                </div>
              )}
          </div>
        )}
				<div className='plapla'>
          <div
            style={{
              width: '100%',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              cursor: 'pointer'
            }}
          >
            {state.row > 0 ? (
              <ChevronLeft style={{ color: '#FFF' }} size={33} onClick={() => handlePage('left')} />
            ) : (
              <div />
            )}
            {state.row < state.max - 1 ? (
              <ChevronRight style={{ color: '#FFF' }} size={33} onClick={() => handlePage('right')} />
            ) : (
              <div />
            )}
          </div>
        </div>
      </Modal>
    </>
  )
}

export default PreviewModal
