import React, { Component } from 'react'
import { Modal, Form, Select, Input, Button } from 'antd'

const { Option } = Select

class UpdateMenuModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      loading: false,
      selectedItems: []
    }
  }  

  componentDidMount () {
    this.props.getRef(this)
  }

  getSelectedItems = (item) => {
    console.log(item)
    this.setState({
      selectedItems: item
    })
  }

  handleChange = (value) => {
    this.setState(() => (
      { selectedItems: value }
    ))
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  render () {
    const imageLink = process.env.REACT_APP_imagesUrl
    const { getFieldsValue, getFieldDecorator, resetFields } = this.props.form
    const { name } = this.props.update
    const handleSubmit = async () => {
      console.log({ ...getFieldsValue(), dishes: this.state.selectedItems })
      resetFields()
      // this.setState({
      //   loading: true
      // })
      // try {
      //   await this.props.onSubmit({ ...getFieldsValue(), dishes: this.state.selectedItems })
      //   this.props.onCancel()
      //   this.setState({
      //     loading: false
      //   })
      //   resetFields()
      // } catch (error) {
      //   this.setState({
      //     loading: false
      //   })
      //   resetFields()
      //   console.error(error)
      // }
    }
    return (
      <>
        <Modal
          title={this.props.title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <Form>
            <Form.Item label='Tên menu'>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'Vui lòng nhập tên menu!' }],
                initialValue: name
              })(
                <Input
                  placeholder='Tên Menu'
                />
              )}
            </Form.Item>
            <Form.Item label='Món ăn trong menu'>
              {getFieldDecorator('dishes', {
                // initialValue: !dishes ? '' : dishes.map(item => item._id)
              })}
              <Select
                value={this.state.selectedItems}
                mode='multiple'
                style={{ width: '100%' }}
                placeholder='Chọn món ăn cho menu'
                onChange={(value) => this.getSelectedItems(value)}
                optionLabelProp='label'
              >
                {this.props.dishes.map((item, index) => (
                  <Option value={item._id} label={item.name} key={index}>
                    <img src={`${imageLink}/${item.imageUrl}`} alt='' width='50px' height='50px' style={{ borderRadius: 5 }} />
                    &nbsp;
                    {item.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Form>
          <div style={{ marginTop: 20 }}>
            <Button
              style={{ float: 'right' }}
              type='primary'
              // loading={() => this.setState({ loading: false })}
              onClick={handleSubmit}
            >
              Lưu
            </Button>
          </div>
          <div style={{ height: 20 }} />
        </Modal>
      </>
    )
  }
}

export default Form.create()(UpdateMenuModal)