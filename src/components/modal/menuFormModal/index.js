import React, { Component, useState } from 'react'
import { Modal, Button } from 'antd'
import MenuForm from './form'

class FormModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      data: null
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  render () {
    return (
      <div style={{ marginBottom: '15px' }}>
        <Modal
          title={this.props.title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <MenuForm menuData={this.state.data} update={!!this.props.update} onSubmit={this.props.onSubmit} onCancel={this.handleCancel} />
        </Modal>
      </div>
    )
  }
}

export default FormModal