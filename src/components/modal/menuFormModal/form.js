import React, { useState } from 'react'
import { Form, Select, Input, Button } from 'antd'
import gql from 'graphql-tag'
import { HOCQueryMutation } from '../../util'

const { Option } = Select

// const handleChange = (value) => {
//   this.setState(() => (
//     { selectedItems: value }
//   ))
// }

function MenuForm (props) {
  const { getFieldsValue, getFieldDecorator, resetFields } = props.form
  const [loading, setLoading] = useState(false)
  const [selectedItems, getSelectedItems] = useState([])

  const handleSubmit = async () => {
    setLoading(true)
    try {
      // if (props.update) {
      //   await props.onSubmit(props.data._id, { ...getFieldsValue(), password: '', image: { data: '', type: '' } })
      // } else {
      //   await props.onSubmit({ ...getFieldsValue(), image: { data: image, type } })
      // }
      if (props.update) {
        await props.onSubmit(props.menuData._id, { ...getFieldsValue() })
      } else {
        await props.onSubmit({ ...getFieldsValue(), dishes: selectedItems })
      }
      props.onCancel()
      setLoading(false)
      resetFields()
    } catch (error) {
      setLoading(false)
      resetFields()
      console.error(error)
    }
  }
  console.log(props)
  return (
    <>
      <Form>
        <Form.Item label='Tên menu'>
          {getFieldDecorator('name', {
            initialValue: props.update ? props.menuData.name : '',
            rules: [{ required: true, message: 'Vui lòng nhập tên menu!' }]
          })(
            <Input
              placeholder='Tên Menu'
            />
          )}
        </Form.Item>
        <Form.Item label='Món ăn trong menu'>
          {getFieldDecorator('dishes', {
            initialValue: props.update ? props.menuData.dishes.map(value => value._id) : []
          })(
            <Select
              mode='multiple'
              style={{ width: '100%' }}
              placeholder='Chọn món ăn cho menu'
              onChange={(value) => getSelectedItems(value)}
              optionLabelProp='label'
            >
              {props.data.dishes.map((item, index) => <Option value={item._id} label={item.name}>{item.name}</Option>)}
            </Select>
          )}
        </Form.Item>
      </Form>
      <div style={{ marginTop: 20 }}>
        <Button
          style={{ float: 'right' }}
          type='primary'
          loading={loading}
          onClick={handleSubmit}
        >
          Lưu
        </Button>
      </div>
      <div style={{ height: 20 }} />
    </>
  )
}

const DISHES = gql`
  query{
    dishes{
      _id
      name
      price
      imageUrl
      isActive
      createdAt
      updatedAt
      }
  }
`

export default HOCQueryMutation([
  {
    query: DISHES,
    name: 'dishes'
  }
])(Form.create()(MenuForm))
