import React, { useState, useEffect } from 'react'
import { Form, Select, Input, Button, InputNumber } from 'antd'
import gql from 'graphql-tag'
import { HOCQueryMutation } from '../../util'

const { Option } = Select

// const handleChange = (value) => {
//   this.setState(() => (
//     { selectedItems: value }
//   ))
// }

function CouponForm (props) {
  console.log(props)
  const { getFieldsValue, getFieldDecorator, resetFields } = props.form
  const [loading, setLoading] = useState(false)
  const [selectedItems, getSelectedItems] = useState([])
  const [dishesForCreateNewCoupon, setDishesForCreateNewCoupon] = useState([])

  const handleSubmit = async () => {
    setLoading(true)
    try {
      // if (props.update) {
      //   await props.onSubmit(props.data._id, { ...getFieldsValue(), password: '', image: { data: '', type: '' } })
      // } else {
      //   await props.onSubmit({ ...getFieldsValue(), image: { data: image, type } })
      // }
      // XỬ LÝ CREATE COUPON
      const { content, percent, dishes } = getFieldsValue()
      const couponValue = { content, percent: percent.toString(), dishes }
      if (props.update) {
        await props.onSubmit(props.menuData._id, couponValue)
      } else {
        await props.onSubmit({ ...getFieldsValue(), dishes: selectedItems })
      }
      props.onCancel()
      setLoading(false)
      resetFields()
    } catch (error) {
      setLoading(false)
      resetFields()
      console.error(error)
    }
  }

  const dishesForUpdate = props.update ? props.menuData.dishes.filter(item => item.couponId != null) : []  

  return (
    <>
      <Form>
        <Form.Item label='Nội dung chương trình giảm giá'>
          {getFieldDecorator('content', {
            initialValue: props.update ? props.menuData.content : '',
            rules: [{ required: true, message: 'Vui lòng nhập nội dung giảm giá!' }]
          })(
            <Input
              placeholder='Nội dung giảm giá'
            />
          )}
        </Form.Item>
        <Form.Item label='Phần trăm giảm giá'>
          {getFieldDecorator('percent', {
            initialValue: props.update ? +props.menuData.percent : '',
            rules: [{ required: true, message: 'Vui lòng nhập phần trăm giảm giá!' }]
          })(
            <InputNumber
              min={0}
              max={100}
              formatter={value => `${value}%`}
              parser={value => value.replace('%', '')}
            />
          )}
        </Form.Item>
        <Form.Item label='Món ăn trong chương trình giảm giá'>
          {getFieldDecorator('dishes', {
            initialValue: props.update ? dishesForUpdate.map(item => item._id) : []
          })(
            <Select
              mode='multiple'
              style={{ width: '100%' }}
              placeholder='Chọn món ăn cho chương trình giảm giá'
              onChange={(value) => getSelectedItems(value)}
              optionLabelProp='label'
            >
              {/* {props.update ? props.menuData.dishes.map((item, index) => <Option value={item._id} label={item.name}>{item.name}</Option>) : props.data.dishesForCoupon.map((item, index) => <Option value={item._id} label={item.name}>{item.name}</Option>)} */}
              {props.update ? props.menuData.dishes.map((item, index) => <Option value={item._id} label={item.name}>{item.name}</Option>) : props.menuData.map((item, index) => <Option value={item._id} label={item.name}>{item.name}</Option>)}

            </Select>
          )}
        </Form.Item>
      </Form>
      <div style={{ marginTop: 20 }}>
        <Button
          style={{ float: 'right' }}
          type='primary'
          loading={loading}
          onClick={handleSubmit}
        >
          Lưu
        </Button>
      </div>
      <div style={{ height: 20 }} />
    </>
  )
}

// const DISHES_FOR_COUPON = gql`
//   query{
//     dishesForCoupon{
//       _id
//       name
//       price
//       imageUrl
//       isActive
//       createdAt
//       updatedAt
//     }
//   }
// `

export default Form.create()(CouponForm)

// export default HOCQueryMutation([
//   {
//     query: DISHES_FOR_COUPON,
//     name: 'dishesForCoupon'
//   }
// ])(Form.create()(CouponForm))

// export default HOCQueryMutation([
//   {
//     query: DISHES,
//     name: 'dishes'
//   }
// ])(Form.create()(CouponForm))
