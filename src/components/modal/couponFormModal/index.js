import React, { Component, useState } from 'react'
import { Modal, Button } from 'antd'
import CouponForm from './form'

class CouponFormModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      data: null
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  render () {
    return (
      <div style={{ marginBottom: '15px' }}>
        <Modal
          title={this.props.title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <CouponForm menuData={this.state.data} update={!!this.props.update} onSubmit={this.props.onSubmit} onCancel={this.handleCancel} />
        </Modal>
      </div>
    )
  }
}

export default CouponFormModal