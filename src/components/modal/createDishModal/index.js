import React, { Component, useState } from 'react'
import { Modal, Button } from 'antd'
import FormCreateDish from './formCreateDish'

// function CreateDishModal() {
//   const [visible, setVisible] = useState(false)

//   function handleOk() {
//     setVisible(false)
//   }

//   function handleCancel() {
//     setVisible(false)
//   }

//   function showModal() {
//     setVisible(true)
//   }

//   return (
//     <div style={{ marginBottom: '15px' }}>
//       <Button type='primary' onClick={showModal}>
//         Thêm món ăn
//       </Button>
//       <Modal
//         title='Thêm món ăn'
//         visible={visible}
//       >
//         <FormCreateDish data={this.props.data} type={this.props.type} update={!!this.props.update} onSubmit={this.props.onSubmit} onCancel={this.handleCancel} />
//       </Modal>
//     </div>
//   )
// }

// export default CreateDishModal


class CreateDishModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false
    }
  }

  componentDidMount () {
    this.props.getRef(this)
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  showModal = () => {
    this.setState({ visible: true })
  }

  render () {
    console.log(this.state.update)
    return (
      <div style={{ marginBottom: '15px' }}>
        <Modal
          title={this.state.type === 'update' ? 'Cập nhật món ăn' : 'Thêm món ăn'}
          // title={this.props.title}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          footer={null}
        >
          <FormCreateDish data={this.state.dishInfo} update={this.state.type} onSubmit={this.props.onSubmit} onCancel={this.handleCancel} />
        </Modal>
      </div>
    )
  }
}

export default CreateDishModal