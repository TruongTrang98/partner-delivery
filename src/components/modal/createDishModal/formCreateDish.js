import React, { useState } from 'react'
import { Input, Button, Form } from 'antd'
import ImageUploader from 'react-images-upload'

import './index.less'

const hasErrors = (fieldsError, fieldsValue) => (
  Object.keys(fieldsError).some(field => fieldsError[field])
  || Object.values(fieldsValue).some(field => !field)
)

function FormCreateDish (props) {
  // console.log('Check update', props.update)
  const { getFieldDecorator, resetFields, getFieldsError, getFieldsValue } = props.form

  const [image, setImage] = useState('')
  const [type, setType] = useState('')
  const [loading, setLoading] = useState(false)

  const getBase64 = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })

  const onDrop = async (picture) => {
    console.log(picture)
    if (picture && picture[0]) {
      let imageBase64 = await getBase64(picture[0])
      console.log(imageBase64)
      setType(picture[0].type)
      setImage(imageBase64)
    } else {
      setImage('')
      setType('')
    }
  }

  const handleSubmit = async () => {
    // console.log(getFieldsValue())
    // console.log({ data: image, type })
    setLoading(true)
    try {
      console.log(props.update === 'update')
      if (props.update === 'update') {
        await props.onSubmit(props.data._id, { ...getFieldsValue(), image: { data: image, type } })
      } else {
        await props.onSubmit({ ...getFieldsValue(), image: { data: image, type } })
      }
      props.onCancel()
      setLoading(false)
      resetFields()
      setType('')
      setImage('')
    } catch (error) {
      setLoading(false)
      resetFields()
      setType('')
      setImage('')
      console.error(error)
    }
  }

  return (
    <>
      <Form>
        <Form.Item label='Tên món ăn'>
          {getFieldDecorator('name', {
            initialValue: props.update === 'update' ? props.data.name : '',
            rules: [{ required: true, message: 'Vui lòng nhập tên món ăn!' }]
          })(
            <Input
              placeholder='Tên món ăn'
            />
          )}
        </Form.Item>
        <Form.Item label='Giá'>
          {getFieldDecorator('price', {
            initialValue: props.update === 'update' ? props.data.price : '',
            rules: [
              { 
                required: true,
                message: 'Vui lòng nhập giá!' },
              {
                pattern: /^-?(0|[1-9][0-9]*)(\.[0-9]*)?$/,
                message: 'Vui lòng nhập số'
              }
            ]
            
          })(
            <Input
              placeholder='Giá'
            />
          )}
        </Form.Item>
        <Form.Item label='Hình ảnh món ăn'>
          <ImageUploader
            withIcon
            singleImage
            withPreview
            buttonText='Chọn file'
            onChange={onDrop}
            buttonClassName={image ? 'disabledUpload' : ''}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            label={'Chọn ảnh từ máy tính của bạn hoặc kéo thả vào đây'}
            labelStyles={{
              width: 200,
              textAlign: 'center'
            }}
            maxFileSize={5242880}
            buttonStyles={{
              color: '#fff',
              borderRadius: 3,
              backgroundColor: '#4ABFA9',
              borderColor: '#4ABFA9',
              textShadow: '0 -1px 0 rgba(0, 0, 0, 0.12)',
              boxShadow: '0 2px 0 rgba(0, 0, 0, 0.045)',
              lineHeight: 1.499,
              position: 'relative',
              display: 'inline-block',
              fontWeight: 400,
              whiteSpace: 'nowrap',
              textAlign: 'center',
              backgroundImage: 'none',
              border: '1px solid transparent'
            }}
          />
        </Form.Item>
      </Form>
      <div style={{ marginTop: 20 }}>
        <Button
          style={{ float: 'right' }}
          type='primary'
          loading={loading}
          onClick={handleSubmit}
          disabled={hasErrors(getFieldsError(), getFieldsValue())}
        >
          Lưu
        </Button>
      </div>
      <div style={{ height: 20 }} />
    </>
  )
}

export default Form.create()(FormCreateDish)