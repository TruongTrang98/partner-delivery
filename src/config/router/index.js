const listPrivateRoute = [
  {
    key: 'newOrder',
    displayName: 'Đơn hàng mới',
    path: '/newOrder',
    component: 'newOrder',
    exact: true
  },
  {
    key: 'orderReceived',
    displayName: 'Đơn hàng đã nhận',
    path: '/orderReceived',
    component: 'orderReceived',
    exact: true
  },
  {
    key: 'orderDone',
    displayName: 'Đơn hàng hoàn thành',
    path: '/orderDone',
    component: 'orderDone',
    exact: true
  },
  {
    key: 'dish',
    displayName: 'Món ăn',
    path: '/dish',
    component: 'dish',
    exact: true,
    icon: 'shop'
  },
  {
    key: 'menu',
    displayName: 'Menu',
    path: '/menu',
    component: 'menu',
    exact: true,
    icon: 'menu'
  },
  {
    key: 'coupon',
    displayName: 'Coupon',
    path: '/coupon',
    component: 'coupon',
    exact: true,
    icon: 'red-envelope'
  }
  // {
  //   key: 'statistics',
  //   displayName: 'Thống kê',
  //   path: '/statistics',
  //   component: 'statistics',
  //   exact: true,
  //   icon: 'unordered-list'
  // }
]

const menu = [
  {
    key: 'subOrder',
    displayName: 'Đơn hàng',
    icon: 'snippets',
    subMenu: [
      {
        key: 'newOrder',
        displayName: 'Đơn hàng mới',
        path: '/newOrder'
      },
      {
        key: 'orderReceived',
        displayName: 'Đơn hàng đã nhận',
        path: '/orderReceived'
      },
      {
        key: 'orderDone',
        displayName: 'Đơn hàng hoàn thành',
        path: '/orderDone'
      }
    ]
  },
  {
    key: 'dish',
    displayName: 'Món ăn',
    path: '/dish',
    icon: 'shop'
  },
  {
    key: 'menu',
    displayName: 'Menu',
    path: '/menu',
    icon: 'menu'
  },
  {
    key: 'coupon',
    displayName: 'Coupon',
    path: '/coupon',
    icon: 'red-envelope'
  }
]
  
export { listPrivateRoute, menu }