// eslint-disable-next-line no-undef
const graphqlUrl = process.env.REACT_APP_graphqlUrl
// eslint-disable-next-line no-undef
const socketEndpoint = process.env.REACT_APP_socketEndpoint
export { graphqlUrl, socketEndpoint }
